/* MongoosePlugin
 *
 * A plugin middleware for AuditLog and Mongoose that automates the propagation of event logging for Mongoose callbacks

 *
 */

const debug = require('debug')('audit-log'),
	crypto = require('crypto'),
	jsondiffpatch= require('jsondiffpatch').create({
		objectHash: function(obj) {
			// this function is used only to when objects are not equal by ref
			return obj._id || obj.id;
		}
	}),
	models= require('mongoose-models'),
	ObjectId = models.mongoose.Schema.Types.ObjectId,
	CustomError= require('error-helper').CustomError;

let MongoosePlugin = function(options) {

	var dbModel;

    this._options = {
        auditLog:null,                                  															// instance of AuditLog
        modelName:'untitled',                           															// string name of model
        namePath:null,                                  															// path to readable object name field
        idPath:'_id',                                   															// path to unique ID field
        versionPath:'__v',                              															// path to mongoose object version number
        debug: false,                                   															// show debug messages
        storeDoc: ['remove'],                           															// name of callbacks that should store document in description field, if any
        excludePaths: ['etag', 'created_at', 'created_by','modified_at', 'modified_by', '__v', '_id', 'tags', 'repricing.history','checks.history'],    		// paths we do not want to track changes, to avoid noise
        deltas: {},                                  																// Copy of the doc itself before applying change...
		events: {},
		analytics: {}
    };

    // override default options with the provided values
    if(typeof options !== 'undefined') {
        for(var attr in options) {
            this._options[attr] = options[attr];
        }
    }

	/*----------------------------------------------*/
	function generateEtag(content) {

		let etag= "",
			hash = crypto.createHash('md5');

		// Do some cleanup...
		content= _.omit(content, self._options.excludePaths);

		if (_.keys(content).length > 0){
			hash.update(JSON.stringify(content));
			etag= hash.digest('hex');
		}

		return etag.toString();
	}

	/*----------------------------------------------*/
	function handleModelChange(savedDoc, cb) {

		let paths, delta;

		dbModel= models.db.model(self._options.modelName);

		if (savedDoc.isNew){
			self._options.deltas[savedDoc._id]= {action: 'create', delta: savedDoc.toObject(), paths: paths};
			return cb(null, 'Create mode, no chnages to track...');
		}

		if(!savedDoc.isModified()){
			return cb(null, 'No changes, nothing to do...');
		}

		// Keep track of modified paths...
		paths= savedDoc.modifiedPaths();

		// Else will retrieve the existing doc from the database...
		dbModel.findById(savedDoc._id).lean().exec(function(err,dbDoc){

			if (err) return cb(err, 'An erro has occured');
			if (!dbDoc) return cb(null, 'Should never see this...');// dunno how this could happen...

debug('tracker.current.etag=', savedDoc.etag);
debug('tracker.db.etag=', dbDoc.etag);

			// Doc has been modified in the meantime....
			if ((savedDoc.etag != dbDoc.etag) && (dbDoc.etag != '__default__')) {

debug('tracker.model=',self._options.modelName);

				let e= new CustomError({model: self._options.modelName, name: 'DocumentExpiredErr', statusCode: 412, message: 'Precondition failed, document is out of date!'});
				return cb(e, 'Document is out of date...');
			}

			// Compute diff between pre doc and save doc...
            delta= jsondiffpatch.diff(_.pick(_.omit(dbDoc,['history']), paths), _.pick(_.omit(savedDoc.toObject(),['history']), paths));

			self._options.deltas[savedDoc._id]= {action: 'update', delta: delta, paths: paths};
			cb(null, 'Model changes tracking done....');
		});
    }

	/*----------------------------------------------*/
	function handleEtagChange(savedDoc, cb) {

		let auditObj= self._options.auditLog,
			content= savedDoc.toObject(),
			isFromV1= savedDoc.isFromV1 || false,
			isFromV2ADS= savedDoc.isFromV2ADS || false,
			changes= self._options.deltas[savedDoc._id] || {},
			delta= changes.delta,
			action= changes.action || '';

		if (action == 'update' && delta){
			delta= _.omit(delta, self._options.excludePaths);
		}

		process.nextTick(function() {

			if (savedDoc.isNew) {
				savedDoc.created_at = savedDoc.modified_at = new Date();
				savedDoc.created_by= auditObj.getUserId();

			debug('generator.current.etag=' + savedDoc.etag);
			savedDoc.etag= generateEtag(content);

				debug('generator.new.etag=' + savedDoc.etag);
				cb(null, 'Etag created...');
			}
			else {
				if (_.keys(delta).length > 0){
					debug('generator.before.etag=' + savedDoc.etag);

					// We do not change it if coming from V1 message...
					// We do not change it if coming from V2 ads week deal stuff...
					if (isFromV1 === false && isFromV2ADS===false){
						savedDoc.modified_at = new Date();
						savedDoc.modified_by= auditObj.getUserId();
					}

					savedDoc.etag= generateEtag(content);

					debug('generator.after.etag=' + savedDoc.etag);
					cb(null, 'Etag modified...');
				}else{
					cb(null, 'Etag unchanged');
				}
			}
		});
	}

	/*----------------------------------------------*/
	function enrichLogEvent(modelName, action, doc, cb) {

		let modelTrims= models.db.model('model_trims'),
			equipmentModels= ['vehicle_equipments', 'make_equipments'];

		if (_.indexOf(equipmentModels, modelName)>=0){
			modelTrims.findOne({id_finition: doc.id_finition}).lean().exec(function(err, trim){
				if (err || !trim) cb(err, null);
				cb(null, {_id_eq: doc._id.toString(), _id_trim: trim._id.toString()});
			});
		}else {
			cb(null, {_id: doc._id.toString()});
		}
	}

	/*----------------------------------------------*/
	function doLogEvent(mode, action, doc, changes, cb){

		const modelName= self._options.modelName,
			description = (action=='update') ? JSON.stringify(changes) : '',
			actionNfo= modelName + '!' + doc[self._options.namePath];

		if (action==='update'){
			debug('---------- DELTA -> ', JSON.stringify(changes));
		}

		if(self._options.storeDoc.indexOf(mode) >= 0 && _.keys(changes).length > 0) {

			enrichLogEvent(modelName, action, doc, function(err, event){
				self._options.auditLog.logEvent(null, 'mongoose', action, actionNfo, event, description, doc[self._options.namePath]);
				if (cb) return cb(err);
			});
		}
		else{
			if (cb){
				return cb(null);
			}
		}
	}

    var self = this;

    /* handler
     *
     * This is a mongoose plugin-able handler function.  Example:
     *
     * var auditFn = auditLog.getPlugin('mongoose', {modelName:'myModel', namePath:'title'});
     * MySchema.plugin(auditFn.handler);
     *
     */
    this.handler = function(schema, options) {

		// Add extra field to schema...
		schema.add({created_by: ObjectId, created_at: Date, modified_at: Date, etag: String, modified_by: ObjectId});

		/*----------------------------------------------*/
        schema.pre('save', function trackChanges(next) {

			var savedDoc = this;

			// Use async stuff instead of parallel middlawre as it did not behave as expected..
			async.series({
				step1: function (cb) {
					handleModelChange(savedDoc, cb);
				},
				step2: function (cb) {
					handleEtagChange(savedDoc, cb);
				}
			},
			function (err, summary) {
				debug('summary=', summary);
				return err ? next(err) : next();
			});
        });

		/*----------------------------------------------*/
        schema.post('save', function applyChanges(doc) {

			let docObj= doc.toObject(),
				modelName= self._options.modelName,
				model= dbModel.db.model(modelName),
				events= self._options.events,
				eventsToSend= [],
				changes = self._options.deltas[doc._id] || {},
				delta= changes.delta,
				action= changes.action;

            if (action == 'update' && delta){

				// Remove unwanted keys just to avoid noise...
				delta= _.omit(delta, self._options.excludePaths);

				// Build events list ...
				_.forIn(events, function(paths,event){
					_.map(paths, function(path){
						if ((_.has(delta, path) || path=='*') && (_.indexOf(eventsToSend, event)<0)){
							eventsToSend.push(event);
						}
					});
				});

				doLogEvent('save', action, docObj, delta, function(err){
					self._options.deltas[docObj._id]= null;
				});
            }
			else {
				// Remove unwanted keys just to avoid noise...
				if (action == 'create'){
					delta= _.omit(docObj, self._options.excludePaths);
					doLogEvent('save', action, docObj, delta);
				}
			}

			// Dispatch events if any...
			_.map(eventsToSend, function(event){
				debug('---------- firing event -> '+ event +'------------');
				model.emit(event, docObj);
			});
        });

        // Remove middleware...
        schema.post('remove', function applyChanges(doc) {

			const docObj = doc.toObject(),
				delta= _.omit(docObj, self._options.excludePaths);

			doLogEvent('remove', 'delete', docObj, delta);
        });
    };
};

exports = module.exports = MongoosePlugin;