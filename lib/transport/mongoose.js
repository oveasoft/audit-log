/* MongooseTransport
 *
 * A MongoDB storage handler for Audit-Log for Node.js
 *
 */
MongooseTransport = function(Schema, options) {
    this.name = 'mongoose';

    this._options = { collectionName:'auditLog', debug: false };
    this._connection= options.mongoConnection; // Re-use existing mongo connection

    // override default options with the provided values
    if(typeof options !== 'undefined') {
        for(var attr in options) {
            this._options[attr] = options[attr];
        }
    }

    this.modelSchema = new Schema({
        actor: {type:String},        
        date: {type:Date},
        origin: {type:String},
        action: {type:String},
        label: {type:String},
        object: {},
        description: {type:String},
        refId: {type:String},
        tags: Array
    });
    
    this.model = this._connection.model(this._options.collectionName, this.modelSchema);

    this.emit = function( dataObject, cb ) {
        this.debugMessage('emit: '+ JSON.stringify(dataObject));
        
        if(dataObject.logType && dataObject.logType == 'Event') {

            if (dataObject.origin == 'express')
                dataObject.refId= 'root';                
            
            var newEvent = new this.model( dataObject );
            newEvent.save(function(err) {
                if(err) return cb(err, null);
                cb(err, newEvent._id);
            });
        }
    }

    this.debugMessage = function(msg) { if(this._options.debug) console.log('Audit-Log(mongoose): '+msg); }

    return this;
}

exports = module.exports = MongooseTransport;